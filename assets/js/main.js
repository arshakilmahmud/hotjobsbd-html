(function($) {
var windowWidth = $(window).width();
$('.navbar-toggle').on('click', function(){
	$('#mobile-nav').slideToggle(300);
});
	
//matchHeightCol
if($('.mHc').length){
  $('.mHc').matchHeight();
};
if($('.mHc1').length){
  $('.mHc1').matchHeight();
};
if($('.mHc2').length){
  $('.mHc2').matchHeight();
};
if($('.mHc3').length){
  $('.mHc3').matchHeight();
};
if($('.mHc4').length){
  $('.mHc4').matchHeight();
};
if($('.mHc5').length){
  $('.mHc5').matchHeight();
};
//$('[data-toggle="tooltip"]').tooltip();

//banner animation
$(window).scroll(function() {
  var scroll = $(window).scrollTop();
  $('.page-banner-bg').css({
    '-webkit-transform' : 'scale(' + (1 + scroll/2000) + ')',
    '-moz-transform'    : 'scale(' + (1 + scroll/2000) + ')',
    '-ms-transform'     : 'scale(' + (1 + scroll/2000) + ')',
    '-o-transform'      : 'scale(' + (1 + scroll/2000) + ')',
    'transform'         : 'scale(' + (1 + scroll/2000) + ')'
  });
});


if($('.fancybox').length){
$('.fancybox').fancybox({
    //openEffect  : 'none',
    //closeEffect : 'none'
  });

}

/*$('.ht-personal-dtls-menu ul li.menu-item-has-children > a').on('click', function(e){
    e.preventDefault();
    $(this).parent().toggleClass('sub-menu-arrow');
    $(this).next().slideToggle(300);

  });*/

/*$('.hdr-contact-info ul li a.hdr-lng-account').on('click', function(){
  $('.hdr-account-menu-cntlr').slideToggle(300);
});*/

$('.hdr-contact-info ul li a.hdr-lng-account').on('click', function(){
    $(this).toggleClass('menu-expend');
    /*$('.hdr-account-menu-cntlr').slideToggle(500);*/
    $('.hdr-account-menu-cntlr').toggleClass('menu-expend1');
  });

$('.ht-personal-dtls-menu ul li.menu-item-has-children > a').on('click', function(){
   $(this).toggleClass('sub-menu-arrow');
   $(this).parent().siblings().find('.ht-personal-dtls-menu ul li.menu-item-has-children > a').removeClass('sub-menu-arrow');
   $(this).parent().find('ul.sub-menu ').slideToggle(300);
   $(this).parent().siblings().find('ul.sub-menu ').slideUp(300);
});

$('.dbdt-dsd-profile').on('click', function(){
    $(this).toggleClass('menu-expend');
    $('.dbdt-dsd-profile ul').slideToggle(500);
  });

/*
------------------
 New Js
------------------
*/
$('.hdr-msg-nti-menu span').on('click', function(){
    $('.hdr-msg-nti-innr').slideToggle(500);
    /*$('.hdr-msg-nti-innr').addClass('hdr-msg-add-cls');*/
    /*$(this).removeClass('hdr-msg-add-cls');*/
  });

 /*$('body,html').click(function(){
   $('.hdr-msg-nti-innr').removeClass('hdr-msg-add-cls');
});*/

$('.hdr-ntifc-menu span').on('click', function(){
    $('.hdr-ntifc-innr').slideToggle(500);
    $('.hdr-ntifc-innr').addClass('hdr-msg-add-cls');
  });


/*
-------------
 New Js
-------------
*/
$('.fl-wedget-title').on('click', function(){
  $(this).toggleClass('thisactive');
  $(this).next('.fl-wedget-item').slideToggle();
});

/*
----------------------
 Tabs Js
----------------------
*/
if( $('.ht-jb-sumry-tbs').length ){
  $('.ht-jb-sumry-tbs:first').show();
  $('.ht-jb-sumry-tbs-menu ul li:first').addClass('active');

  $('.ht-jb-sumry-tbs-menu ul li').on('click',function(){
    index = $(this).index();
    $('.ht-jb-sumry-tbs-menu ul li').removeClass('active');
    $(this).addClass('active');
    $('.ht-jb-sumry-tbs').hide();
    $('.ht-jb-sumry-tbs').eq(index).show();
  });
}



/*
------------------ 
 Menu Js
------------------
*/
if (windowWidth <= 991) {
  $('.hambergar-icon').on('click', function(e){
    $('.main-nav').slideToggle(500);
    $(this).toggleClass('cross-icon');
  });

  $('li.menu-item-has-children > a').on('click', function(e){
    e.preventDefault();
    $(this).parent().toggleClass('sub-menu-arrow');
    $(this).next().slideToggle(300);

  });
}

/*
----------------------
 Tabs Js
----------------------
*/
if( $('.ht-login-rgi-form-tabs').length ){
  $('.ht-login-rgi-form-tabs:first').show();
  $('.ht-login-rgi-tabs ul li:first').addClass('active');

  $('.ht-login-rgi-tabs ul li').on('click',function(){
    index = $(this).index();
    $('.ht-login-rgi-tabs ul li').removeClass('active');
    $(this).addClass('active');
    $('.ht-login-rgi-form-tabs').hide();
    $('.ht-login-rgi-form-tabs').eq(index).show();
  });
}

/*
----------------------
 Tabs Js
----------------------
*/
if( $('.ht-cplnes-tbbs').length ){
  $('.ht-cplnes-tbbs:first').show();
  $('.ht-cplnes-tbbs-menu ul li:first').addClass('active');

  $('.ht-cplnes-tbbs-menu ul li').on('click',function(){
    index = $(this).index();
    $('.ht-cplnes-tbbs-menu ul li').removeClass('active');
    $(this).addClass('active');
    $('.ht-cplnes-tbbs').hide();
    $('.ht-cplnes-tbbs').eq(index).show();
  });
}

/*
----------------------
 Tabs Js
----------------------
*/
if( $('.ht-personal-tabs').length ){
  $('.ht-personal-tabs:first').show();
  $('.ht-personal-dtls-tbs-menu ul li:first').addClass('active');

  $('.ht-personal-dtls-tbs-menu ul li').on('click',function(){
    index = $(this).index();
    $('.ht-personal-dtls-tbs-menu ul li').removeClass('active');
    $(this).addClass('active');
    $('.ht-personal-tabs').hide();
    $('.ht-personal-tabs').eq(index).show();
  });
}

if( $('.ht-dashboard-tabs').length ){
  $('.ht-dashboard-tabs:first').show();
  $('.ht-dashboard-tabs-menu ul li:first').addClass('active');

  $('.ht-dashboard-tabs-menu ul li').on('click',function(){
    index = $(this).index();
    $('.ht-dashboard-tabs-menu ul li').removeClass('active');
    $(this).addClass('active');
    $('.ht-dashboard-tabs').hide();
    $('.ht-dashboard-tabs').eq(index).show();
  });
}
if( $('.new-jobs-tab').length ){
  $('.new-jobs-tab:first').show();
  $('.new-jobs-tab-menu ul li:first').addClass('active');

  $('.new-jobs-tab-menu ul li').on('click',function(){
    index = $(this).index();
    $('.new-jobs-tab-menu ul li').removeClass('active');
    $(this).addClass('active');
    $('.new-jobs-tab').hide();
    $('.new-jobs-tab').eq(index).show();
  });
}

if( $('.my-tning-tabs').length ){
  $('.my-tning-tabs:first').show();
  $('.my-tning-tabs-menu ul li:first').addClass('active');

  $('.my-tning-tabs-menu ul li').on('click',function(){
    index = $(this).index();
    $('.my-tning-tabs-menu ul li').removeClass('active');
    $(this).addClass('active');
    $('.my-tning-tabs').hide();
    $('.my-tning-tabs').eq(index).show();
  });
}

if( $('.em-sms-tbl-tabs').length ){
  $('.em-sms-tbl-tabs:first').show();
  $('.em-sms-tbl-tbs-menu ul li:first').addClass('active');

  $('.em-sms-tbl-tbs-menu ul li').on('click',function(){
    index = $(this).index();
    $('.em-sms-tbl-tbs-menu ul li').removeClass('active');
    $(this).addClass('active');
    $('.em-sms-tbl-tabs').hide();
    $('.em-sms-tbl-tabs').eq(index).show();
  });
}
/*
----------------------
 Tabs Js
----------------------
*/
if( $('.ht-sidebar-tbs-grid-item').length ){
  $('.ht-sidebar-tbs-grid-item:first').show();
  $('.ht-sidebar-menu ul li:first').addClass('active');

  $('.ht-sidebar-menu ul li').on('click',function(){
    index = $(this).index();
    $('.ht-sidebar-menu ul li').removeClass('active');
    $(this).addClass('active');
    $('.ht-sidebar-tbs-grid-item').hide();
    $('.ht-sidebar-tbs-grid-item').eq(index).show();
  });
}



/*
----------------------
 Tabs Js
----------------------
*/
/*if( $('.em-cp-service-tbs').length ){
  $('.em-cp-service-tbs:first').show();
  $('.em-cp-service-tbs-menu ul li:first').addClass('active');

  $('.em-cp-service-tbs-menu ul li').on('click',function(){
    index = $(this).index();
    $('.em-cp-service-tbs-menu ul li').removeClass('active');
    $(this).addClass('active');
    $('.em-cp-service-tbs').hide();
    $('.em-cp-service-tbs').eq(index).show();
  });
}*/

 /*
----------------------
 Tabs Js
----------------------
*/
if( $('.quantity-price-tbs').length ){
  $('.quantity-price-tbs:first').show();
  $('.em-cp-service-tbs-btm-menu ul li:first').addClass('active');

  $('.em-cp-service-tbs-btm-menu ul li').on('click',function(){
    index = $(this).index();
    $('.em-cp-service-tbs-btm-menu ul li').removeClass('active');
    $(this).addClass('active');
    $('.quantity-price-tbs').hide();
    $('.quantity-price-tbs').eq(index).show();
  });
}

/*
----------------------
 Tabs Js
----------------------
*/
if( $('.em-cp-service-tbs-btm').length ){
  $('.em-cp-service-tbs-btm:first').show();
  $('.em-cp-listing-menu ul li:first').addClass('active');

  $('.em-cp-listing-menu ul li').on('click',function(){
    index = $(this).index();
    $('.em-cp-listing-menu ul li').removeClass('active');
    $(this).addClass('active');
    $('.em-cp-service-tbs-btm').hide();
    $('.em-cp-service-tbs-btm').eq(index).show();
  });
}



/*
----------------------
 Tabs Js
----------------------
*/
if( $('.jb-pst-chk-tbs').length ){
  $('.jb-pst-chk-tbs:first').show();
  $('.jb-pst-tabs-menu ul li:first').addClass('active');

  $('.jb-pst-tabs-menu ul li').on('click',function(){
    index = $(this).index();
    $('.jb-pst-tabs-menu ul li').removeClass('active');
    $(this).addClass('active');
    $('.jb-pst-chk-tbs').hide();
    $('.jb-pst-chk-tbs').eq(index).show();
  });
}

/*
----------------------
 Tabs Js
----------------------
*/
if( $('.JbPstChkTbs').length ){
  $('.JbPstChkTbs:first').show();
  $('#JbTbsMenu ul li:first').addClass('active');

  $('#JbTbsMenu ul li').on('click',function(){
    index = $(this).index();
    $('#JbTbsMenu ul li').removeClass('active');
    $(this).addClass('active');
    $('.JbPstChkTbs').hide();
    $('.JbPstChkTbs').eq(index).show();
  });
}

/*
----------------------
 Tabs Js
----------------------
*/
if( $('.CmModalTabs').length ){
  $('.CmModalTabs:first').show();
  $('.cmn-modal-menu ul li:first').addClass('active');

  $('.cmn-modal-menu ul li').on('click',function(){
    index = $(this).index();
    $('.cmn-modal-menu ul li').removeClass('active');
    $(this).addClass('active');
    $('.CmModalTabs').hide();
    $('.CmModalTabs').eq(index).show();
  });
}

var readURL = function(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.profile-pic').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}


$(".file-upload").on('change', function(){
    readURL(this);
});

$(".upload-button").on('click', function() {
   $(".file-upload").click();
});









/*$('#myModal').modal(options);*/

/*
--------------------
  Accordion Js
--------------------
*/
$('.mct-faq-accordion-title').click(function(){
    $(this).next().slideToggle(300);
    $(this).parent().siblings().find('.mct-faq-accordion-des').slideUp(300);
    $(this).toggleClass('active');
    $(this).parent().siblings().find('.mct-faq-accordion-title').removeClass('active');
});

$('.ht-faq-accordion-title').click(function(){
    $(this).next().slideToggle(300);
    $(this).parent().siblings().find('.ht-faq-accordion-des').slideUp(300);
    $(this).toggleClass('active');
    $(this).parent().siblings().find('.ht-faq-accordion-title').removeClass('active');
});

/**/
if( $('#container5').length ){
var bar5 = new ProgressBar.Circle(container5, {
  color: '#334e6f',
  // This has to be the same size as the maximum width to
  // prevent clipping
  strokeWidth: 15,
  trailWidth: 15,
  easing: 'easeInOut',
  duration: 1400,
  text: {
    autoStyleContainer: false
  },
  from: { color: '#334e6f', width: 15 },
  to: { color: '#00732e', width: 15 },
  // Set default step function for all animate calls
  step: function(state, circle) {
    circle.path.setAttribute('stroke', state.color);
    circle.path.setAttribute('stroke-width', state.width);

    var value = Math.round(circle.value() * 100);
    if (value === 0) {
      circle.setText(0);
    } else {
      circle.setText(80);
    }

  }
});
bar5.animate(0.68);  // Number from 0.0 to 1.0

}

if( $('#container6').length ){
var bar5 = new ProgressBar.Circle(container6, {
  color: '#334e6f',
  // This has to be the same size as the maximum width to
  // prevent clipping
  strokeWidth: 15,
  trailWidth: 15,
  easing: 'easeInOut',
  duration: 1400,
  text: {
    autoStyleContainer: false
  },
  from: { color: '#334e6f', width: 15 },
  to: { color: '#00732e', width: 15 },
  // Set default step function for all animate calls
  step: function(state, circle) {
    circle.path.setAttribute('stroke', state.color);
    circle.path.setAttribute('stroke-width', state.width);

    var value = Math.round(circle.value() * 100);
    if (value === 0) {
      circle.setText(0);
    } else {
      circle.setText(80);
    }

  }
});
bar5.animate(0.68);  // Number from 0.0 to 1.0

}

//products counter
if( $('.qty').length ){
  $('.qty').each(function() {
    var spinner = $(this),
      input = spinner.find('input[type="number"]'),
      btnUp = spinner.find('.plus'),
      btnDown = spinner.find('.minus'),
      min = 1,
      max = input.attr('max');

    btnUp.click(function() {
      var oldValue = parseFloat(input.val());
      if (oldValue <= max) {
        var newVal = oldValue;
      } else {
        var newVal = oldValue + 1;
      }
      spinner.find("input").val(newVal);
      spinner.find("input").trigger("change");
    });

    btnDown.click(function() {
      var oldValue = parseFloat(input.val());
      if (oldValue <= min) {
        var newVal = oldValue;
      } else {
        var newVal = oldValue - 1;
      }
      spinner.find("input").val(newVal);
      spinner.find("input").trigger("change");
    });

  });

}

/*
-------------------
 Skillbar Js
-------------------
*/
if( $('.skillbar').length ){
  
  $('.skillbar').skillBars({
    });
}

/*$('body,html').on('click', function(){
   $('.ht-brsw-cus-menu-wrp').removeClass('show-menu');
 });*/
$('.brw-cus-menu').on('click', function(){
  $('.ht-brsw-cus-menu-wrp').addClass('show-menu');
});
$('.close-icon').on('click', function(){
  $('.ht-brsw-cus-menu-wrp').removeClass('show-menu');
});


/*
-----------------
 Edit Js
-----------------
*/

$('.ht-prls-dtls-frm-tbl button').on('click', function(){
  $('.ht-personal-dtls').show();
});
$('.ht-prls-dtls-frm-tbl button').on('click', function(){
  $('.ht-prls-dtls-frm-tbl').hide();
});

/*$('.ht-jb-cp-ck ul').on('click', function(){
  $('.ht-jb-toggle').show();
});
$('.ht-jb-cp-ck ul').on('click', function(){
  $('.ht-jb-toggle').hide();
});*/

/*$('.ht-jb-cp-ck').on('click', function(e){
  $('.ht-jb-toggle').hide('ht-jb-toggle-cls');
});
$('.ht-jb-cp-ck').on('click', function(e){
  $('.ht-jb-toggle').show('ht-jb-toggle-cls');
});*/

/*$('.ht-jb-cp-ck').on('click', function(){
  $('.ht-jb-toggle').show(500);
});

$('.ht-jb-cp-ck').on('click', function(){
  $('.ht-jb-toggle').hide(500);
});*/

$('.ht-jb-cp-ck').on('click', function(){
  $('.ht-jb-toggle').slideDown(300);
});

/*$('.brw-cus-menu').on('click', function(){
  $('.ht-brsw-cus-menu-wrp').addClass('show-menu');
});
$('.close-icon').on('click', function(){
  $('.ht-brsw-cus-menu-wrp').removeClass('show-menu');
});*/
/*$('#HtPrlsDtlsTbl1 button').on('click', function(){
  $('.ht-personal-dtls1').show();
});
$('#HtPrlsDtlsTbl1 button').on('click', function(){
  $('.ht-personal-dtls1').hide();
});*/
/*$('.ht-prls-dtls-frm-tbl button').on('click', function(){
     $('.ht-prls-dtls-frm-tbl').toggleClass('sub-menu-arrow');
     $(this).parent().siblings().find('.ht-prls-dtls-frm-tbl').removeClass('sub-menu-arrow');
     $(this).parent().find('.ht-personal-dtls').slideToggle(300);
     $(this).parent().siblings().find('.ht-personal-dtls').slideUp(300);
 });*/


/*$('.yesclass').on('click', function(e){
  $('.clickhide').addClass('opacity-1');
});
$('.noclass').on('click', function(e){
  $('.clickhide').removeClass('opacity-1');
});*/

if( $('.ClickTabs').length ){
  $('.ClickTabs:first').show();
  $('.filter-check-row:first').addClass('active');

  $('.filter-check-row').on('click',function(){
    index = $(this).index();
    $('.filter-check-row').removeClass('active');
    $(this).addClass('active');
    $('.ClickTabs').hide();
    $('.ClickTabs').eq(index).show();
  });
}





/*
----------------------
 Tabs Js
----------------------
*/
if( $('.hm-job-cty-tab').length ){
  $('.hm-job-cty-tab:first').show();
  $('.hm-job-cty-tab-menu ul li:first').addClass('active');

  $('.hm-job-cty-tab-menu ul li').on('click',function(){
    index = $(this).index();
    $('.hm-job-cty-tab-menu ul li').removeClass('active');
    $(this).addClass('active');
    $('.hm-job-cty-tab').hide();
    $('.hm-job-cty-tab').eq(index).show();
  });
}

/*
----------------------
 Tabs Js
----------------------
*/
if( $('.wrk-sp-grid-cntlr').length ){
  $('.wrk-sp-grid-cntlr:first').show();
  $('.wrk-sp-tbs-wrp ul li:first').addClass('active');

  $('.wrk-sp-tbs-wrp ul li').on('click',function(){
    index = $(this).index();
    $('.wrk-sp-tbs-wrp ul li').removeClass('active');
    $(this).addClass('active');
    $('.wrk-sp-grid-cntlr').hide();
    $('.wrk-sp-grid-cntlr').eq(index).show();
  });
}

/*
----------------------
 Tabs Js
----------------------
*/
if( $('.fl-jb-pst-tbs').length ){
  $('.fl-jb-pst-tbs:first').show();
  $('.fl-jb-pst-tbs-wrp ul li:first').addClass('active');

  $('.fl-jb-pst-tbs-wrp ul li').on('click',function(){
    index = $(this).index();
    $('.fl-jb-pst-tbs-wrp ul li').removeClass('active');
    $(this).addClass('active');
    $('.fl-jb-pst-tbs').hide();
    $('.fl-jb-pst-tbs').eq(index).show();
  });
}


/*
----------------
 Hm Banner Slider
----------------
*/
if( $('.HmBnSlider').length ){
  $('.HmBnSlider').slick({
    dots: true,
    arrows: true,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 4000,
    speed: 700,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: $('.hm-prev-nxt .fl-nxt-prev .fl-prev'),
    nextArrow: $('.hm-prev-nxt .fl-nxt-prev .fl-next'),
    
  });
}

/*
----------------
 Banner Slider
----------------
*/
if( $('.hm-govt-job-slider').length ){
  $('.hm-govt-job-slider').slick({
    dots: false,
    arrows: true,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 4000,
    speed: 700,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: $('.hm-gv-jb-prev-nxt .fl-nxt-prev .fl-prev'),
    nextArrow: $('.hm-gv-jb-prev-nxt .fl-nxt-prev .fl-next'),
    
  });
}

if( $('.learning-slider').length ){
  $('.learning-slider').slick({
    dots: false,
    arrows: true,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 4000,
    speed: 700,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: $('.hm-prev-nxt .fl-nxt-prev .fl-prev'),
    nextArrow: $('.hm-prev-nxt .fl-nxt-prev .fl-next'),
  });
}

if( $('.NewTrainerSlider').length ){
  $('.NewTrainerSlider').slick({
    dots: false,
    arrows: true,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 4000,
    speed: 700,
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: $('.hm-prev-nxt .fl-nxt-prev .fl-prev'),
    nextArrow: $('.hm-prev-nxt .fl-nxt-prev .fl-next'),
    responsive: [
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 575,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
  });
}

if( $('.our-client-slider').length ){
  $('.our-client-slider').slick({
    dots: true,
    arrows: false,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 4000,
    speed: 700,
    slidesToShow: 1,
    slidesToScroll: 1
  });
}


if( $('.hm-download-slider').length ){
  $('.hm-download-slider').slick({
    dots: true,
    arrows: false,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 4000,
    speed: 700,
    slidesToShow: 1,
    slidesToScroll: 1
  });
}


if( $('.hm-add-slider').length ){
  $('.hm-add-slider').slick({
    dots: false,
    arrows: false,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 4000,
    speed: 700,
    slidesToShow: 1,
    slidesToScroll: 1
  });
}

if( $('.ht-jb-service-slider').length ){
  $('.ht-jb-service-slider').slick({
    dots: true,
    arrows: true,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 4000,
    speed: 700,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: $('.hm-gv-jb-prev-nxt .fl-nxt-prev .fl-prev'),
    nextArrow: $('.hm-gv-jb-prev-nxt .fl-nxt-prev .fl-next'),
  });
}

if( $('.ht-ctn-slider-wrp').length ){
  $('.ht-ctn-slider-wrp').slick({
    dots: true,
    arrows: false,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 4000,
    speed: 700,
    slidesToShow: 2,
    slidesToScroll: 1,
    responsive: [
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
      
      ]
  });
}

/**
Responsive on 767px
*/
/*
----------------------
 Responsive Js
----------------------
*/

// if (windowWidth <= 767) {

   $('.fl-wedget-title1').on('click', function(e){
    e.preventDefault();
    $(this).parent().toggleClass('sub-menu-arrow');
    $(this).next().slideToggle(300);

  });



// if (windowWidth <= 767) {
  $('.toggle-btn').on('click', function(){
    $(this).toggleClass('menu-expend');
    $('.toggle-bar ul').slideToggle(500);
  });


// }



// http://codepen.io/norman_pixelkings/pen/NNbqgG
// https://stackoverflow.com/questions/38686650/slick-slides-on-pagination-hover


/**
Slick slider
*/
if( $('.responsive-slider').length ){
    $('.responsive-slider').slick({
      dots: true,
      infinite: false,
      speed: 300,
      slidesToShow: 4,
      slidesToScroll: 4,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
}



/*
------------------
 New Js
------------------
*/
$('.wlc-cv-bnk-cl-btm').on('click', function(){
  $('.wlc-cv-bnk-cl-op-show').addClass('show-menu');
});
$('.wlc-cv-bnk-cl-op-cls').on('click', function(){
  $('.wlc-cv-bnk-cl-op-show').removeClass('show-menu');
});









/*Google Map*/
var CustomMapStyles  = [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]

/*
-----------------------
Start Contact Google Map ->> 
-----------------------
*/
if( $('#googlemap').length ){
    var latitude = $('#googlemap').data('latitude');
    var longitude = $('#googlemap').data('longitude');

    var myCenter= new google.maps.LatLng(latitude,  longitude);
    var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
    function initialize(){
        var mapProp = {
          center:myCenter,

          mapTypeControl:false,
          scrollwheel: false,

          zoomControl: false,
          disableDefaultUI: true,
          zoom:17,
          streetViewControl: false,
          rotateControl: false,
          mapTypeId:google.maps.MapTypeId.ROADMAP,
          styles : CustomMapStyles
      };
      var map= new google.maps.Map(document.getElementById('googlemap'),mapProp);
    }

    google.maps.event.addDomListener(window, 'load', initialize);
}

    new WOW().init();

})(jQuery);